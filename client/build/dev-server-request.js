/**
 * Created by jinzhengkun on 17/1/10.
 */
var proxyMiddleware = require('http-proxy-middleware')

var apiContext = '/api/dream'
var apiServer = 'http://127.0.0.1:3000'
var token = '111'

var proxy = new proxyMiddleware(apiContext, {
  // apiContext: {}, // 替换api前缀
  target: apiServer, // 服务器地址
  changeOrigin: true,
  header: { // header头
    Authorization: 'Bearer ' + token // token验证
  }
})



var staticServer = 'http://127.0.0.1:8888'
var proxy1 = new proxyMiddleware('/vendor', {
  target: staticServer, // 服务器地址
  changeOrigin: true
})
module.exports = [proxy, proxy1]
