/**
 * Created by jinzhengkun on 17/1/10.
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.config.debug = true
Vue.use(VueRouter)

import welcome from './views/welcome'
import login from './views/login'
import index from './views/index'
import register from './views/register'

const router = new VueRouter({
  root: '/dream/',
  // hashbang: true,
  routes: [
    {
      path: '/',
      component: welcome,
      name: 'welcome'
    },
    {
      path: '/login',
      component: login,
      name: 'login'
    },
    {
      path: '/index',
      component: index,
      name: 'index'
    },
    {
      path: '/register',
      component: register,
      name: 'register'
    }
  ]
})

export default router
