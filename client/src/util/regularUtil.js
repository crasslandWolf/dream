/**
 * Created by jinzhengkun on 17/1/17.
 */
var emailRegExp = /^(\w)+(\.\w+)*@(\w)+((\.\w+)+)$/

module.exports = {
  emailRegExp: emailRegExp
}
