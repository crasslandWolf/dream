/**
 * Created by jinzhengkun on 17/1/14.
 */
var d = new Date()
var a = ['星期天', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六']
export default {
  getCustomFullDay (split) {
    split = split || '-'
    return d.getFullYear() + split + fixTimeNum(d.getMonth() + 1) + split + fixTimeNum(d.getDate())
  },
  getWeek () {
    return a[d.getDay()]
  },
  getCurrentTime (split) {
    d = new Date()
    split = split || ':'
    return fixTimeNum(d.getHours()) + split + fixTimeNum(d.getMinutes()) + split + fixTimeNum(d.getSeconds())
  },
  getDateAndTime (icon, split) {
    d = new Date()
    split = split || ':'
    icon = icon || '-'
    return d.getFullYear() + icon + fixTimeNum(d.getMonth() + 1) + icon + fixTimeNum(d.getDate()) + ' ' + fixTimeNum(d.getHours()) + split + fixTimeNum(d.getMinutes()) + split + fixTimeNum(d.getSeconds())
  }
}
function fixTimeNum (num) {
  if (num < 10 || num.length < 2) {
    num = '0' + num
  }
  return num
}
