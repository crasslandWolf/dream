// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'
// import 'bootstrap/dist/css/bootstrap.css'

Vue.use(VueResource)

Vue.http.options.root = '/api/dream'
Vue.http.options.emulateJSON = true

Vue.http.interceptors.push((request, next) => {
  console.log('cao')
  let separator = request.url.indexOf('?') === -1 ? '?' : '&'
  console.log(request.method)
  if (request.method === 'get' || request.method === 'GET') {
    request.url = request.url + separator + 'noCache=' + new Date().getTime() + '&version=1.0'
  } else {
    request.url = request.url + separator + 'version=1.0'
  }
  next()
})
console.log('哈哈哈哈哈')
Vue.http.headers.common['authorization'] = 'Bearer ' + '23123123123123'  // 如果url中直接存在token,则在初始化的时候就加上.如果是在微信企业号里面,则为了加载友好,在第一个页面中设置token
Vue.http.headers.common['x-api-server'] = '1312'
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router: router,
  ...App
})
