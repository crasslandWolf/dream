/**
 * Created by jinzhengkun on 17/1/10.
 * 指定链接的数据库
 * 配置
 * 使用mongoose, mongoose是在node.js异步环境下对mongodb进行便捷操作的对象模型工具
 */

var mongoose = require('mongoose')
var DB_URL = 'mongodb://localhost/dream'
/**
 * 链接
 */
mongoose.connect(DB_URL)

/**
 * 链接成功
 */
mongoose.connection.on('connected', function () {
    console.log('Mongoose connection open to: ' + DB_URL)
})

/**
 * 链接异常
 */

mongoose.connection.on('error', function (err) {
    console.log('Mongoose connection error: ' + err)
})

/**
 * 断开链接
 */

mongoose.connection.on('disconnected', function () {
    console.log('Mongoose connection disconnected')
})
exports.mongoose = mongoose