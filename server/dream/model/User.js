/**
 * Created by jinzhengkun on 17/1/13.
 */

/**
 * schema是mongoose里会用到的一种数据模式，可以理解为表结构的定义；每个schema会映射到mongodb中的一个collection，它不具备操作数据库的能力
 */

var mongodb = require('./mongodb-config')

var Schema = mongodb.mongoose.Schema

// 定义 user 的数据结构
// 创建 Schema
var userSchma = new Schema({
    username: {type: String, unique: false, required: true},
    name: {type: String, unique: false, required: false},
    password: {type: String, unique: false, required: true},
    age: {type: Number, unique: false, required: false},
    sex: {type: String, unique: false, required: false},
    phone: {type: String, unique: false, required: false},
    email: {type: String, unique: true, required: true},
    create_at: {type: Date, unique: false, default: Date.now},
    create_time: {type: Number, unique: false}
})
/**
 * 给 user 添加用户认证
 * @type {*}
 */


// 现在的 Schema 没有什么用，需要在它的基础上创建一个模型(model)

var User = mongodb.mongoose.model("user", userSchma)

// 模拟创建一个user类, 在其原型上添加操作方法
var UserDAO = function () {}

// 获取全部
UserDAO.prototype.find = function (paramsObj, callback) {
    User.find(paramsObj, function (err, result) {
        callback(err, result)
    })
}

// 获取唯一
UserDAO.prototype.findOne = function (paramsObj, callback) {
    User.findOne(paramsObj, function (err, result) {
        callback(err, result)
    })
}

// 新增
UserDAO.prototype.insert = function (formObj, callback) {
    var user = new User({
        username: formObj.username || '',
        name: formObj.name || '',
        password: formObj.password || '',
        age: formObj.age || 0,
        sex: formObj.sex || '',
        phone: formObj.phone || '',
        email: formObj.email || '',
        create_at: formObj.create_at || Date.now,
        create_time: formObj.create_time || ''
    })
    user.save(function (err, result) {
        if (err) {
            callback(err, err)
        }
        else {
            callback(false, result)
        }
    })
}
module.exports = new UserDAO()