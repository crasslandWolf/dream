/**
 * Created by jinzhengkun on 17/1/16.
 */
var express = require('express')
var router = express.Router()

var user = require('../controller/User')
// var passport = require('passport')

var passport = require('../jwt/jwt').passport


// 登录
router.post('/login', user.login)
// 获取
router.get('/getUsers', passport.authenticate('jwt', { session: false }), user.getUsers)
// 注册
router.post('/register', user.register)
// 检查邮箱是否被注册过
router.post('/hasAccount', user.hasAccount)
module.exports = router