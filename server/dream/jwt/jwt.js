/**
 * Created by jinzhengkun on 17/1/11.
 */
// 这里设置jwt验证的配置
/**
 * Json Web Token简称JWT，用来在服务器端和客户端传递数据。JWT是由三段处理后的字符串通过点号组成，看起来有点长，如下：
 *
 * eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1NTZiZWRmNDhmOTUzOTViMTlhNjc1ODgiLCJlbWFpbCI6InNpbW9uQGZ1bGxzdGFja3RyYWluaW5nLmNvbSIsIm5hbWUiOiJTaW1vbiBIb2xtZXMiLCJleHAiOjE0MzUwNDA0MTgsImlhdCI6MTQzNDQzNTYxOH0.GD7UrfnLk295rwvIrCikbkAKctFFoRCHotLYZwZpdlE
 * 第一段是一个编码之后的json对象，这个json对象包含了hash算法和类型。
 * 第二段也是一个编码之后的json对象，也就是我们需要的令牌数据。
 * 第三段是一个签名，签名的密码保存在服务端。
 * 所以这个长长的字符串有两段是没有加密的，只是编码。
 * 这样便于浏览器可以方便的解码而获取到信息，现代的浏览器会有一个atob()的方法来解码Base64的字符串，对应的有一个btoa()方法来编码。
 * 而第三部分的签名可以确保信息没有被篡改，前提是保护好服务器端的密钥。 我们将用它来传递认证之后的用户信息。
 */

// var bcrypt = require('bcrypt-nodejs') // 密码加密
var jwt = require('jsonwebtoken')
var passport = require('passport')
var passportJWT = require('passport-jwt')
var User = require('../model/User')


var EXPIRES_IN_MINUTES = '7d' // 设置过期时间,7天
var ALCORITHM = 'HS256' // 设置算法
var ISSUER = 'crasslandWolf.com' //
var AUDIENCE = 'crasslandWolf.com'

/**
 * 生成token, 使用用户的id,email,密码
 * @param user
 * @returns {*}
 */
function createToken (user, secret) {
    var userInfo = {
        id: user._id,
        email: user.email,
        password: user.password
    }
    var secret = secret
    var signature = {
        algorithm: ALCORITHM,
        expiresIn: EXPIRES_IN_MINUTES,
        issuer: ISSUER,
        audience: AUDIENCE
    }
    return jwt.sign(userInfo, secret, signature)
}

/**
 * 去数据库验证合不合法
 */
var ExtractJwt = passportJWT.ExtractJwt
var JwtStrategy = passportJWT.Strategy

var jwtOptions = {}

jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeader()
jwtOptions.secretOrKey = 'crassland_wolf-secret'

var strategy = new JwtStrategy(jwtOptions, function (jwt_payload, callback) {
    // 验证用户合不合发
    var jwt_payload_obj = {
        id: jwt_payload._id,
        email: jwt_payload.email,
        password: jwt_payload.password
    }
    User.findOne(jwt_payload_obj, function (err, result) {
        if (err) {
            callback(null, false, {message: '服务器报错'})
            return
        }
        if (!result) {
            callback(null, false, {message: 'token解析后数据库验证失败'})
        }
        else {
            callback(null, result, {message: '请求成功'})
        }
    })
})

passport.use(strategy)
// 生成hash
module.exports = {
    createToken: createToken,
    passport: passport
}