/**
 * Created by jinzhengkun on 17/1/11.
 */

// mongodb 的查询，全部查询或者按照某个字段查询，这里用一个方法，前端请求查询的时候只需传指定的参数就好
module.exports = {
    // 查询去掉时间戳和版本号
    httpGetQuery: function (obj) {
        for (e in obj) {
            if (e == 'noCache' || e == 'version') {
                delete obj[e]
            }
        }
        return obj
    },
    // 服务器请求失败返回失败信息
    errorResponse: {
        status: false,
        message: '获取失败'
    },
    // 登录判断，首先判断有没有这个账户，然后判断密码是不是正确，正确生成token返回
    // 还要加token验证，一步一步来
    loginConfig: function (obj) {
        for (e in obj) {
            if (e == 'password') {
                delete obj[e]
            }
        }
        return obj
    },
    noAccountResponse: {
        success: true, // 请求成功，服务器正常返回
        status: false, // 获取数据为空标志
        message: '账户不存在'
    },
    passwordErrorResponse: {
        success: true, // 请求成功，服务器正常返回
        status: false, // 获取数据为空标志/或者操作不成功
        message: '密码不正确'
    },
    sendJsonResponse: function (res, status, result) {
        res.satus(status)
        res.json(result)
    },
    hasAccountResponse: {
      success: true,
      status: false,
      message: '该邮箱已被注册'
    },
    noHasAccountResponse: {
        success: true,
        status: true,
        message: '该邮箱没被注册'
    },
    HTTP_STATUS_POST: 201,
    HTTP_STATUS_GET: 200,
    HTTP_STATUS_PUT: 200,
    HTTP_STATUS_DELETE: 204,
    HTTP_STATUS_ERROR: 422
}