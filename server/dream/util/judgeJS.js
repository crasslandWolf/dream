/**
 * Created by jinzhengkun on 17/1/10.
 */


module.exports = {
    isObjectEmpty: function (obj) {
        for (e in obj) {
            return false
        }
        return true
    },
    deepClone: function (obj) {
        return deepClone(obj)
    }
}

function deepClone (sObj){
    if(typeof sObj !== "object"){
        return sObj;
    }
    var s = {};
    if(sObj.constructor == Array){
        s = [];
    }
    for(var i in sObj){
        s[i] = deepClone(sObj[i]);
    }
    return s;
}

