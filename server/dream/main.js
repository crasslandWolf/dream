/**
 * Created by jinzhengkun on 17/1/10.
 */

var app = require('../express')

var API = require('./api/api')

var passport = require('./jwt/jwt').passport


app.use(passport.initialize())
// 统一api上下文
var apiRouter = '/api/dream'
app.use(apiRouter, API);
/**
 * 处理请求错误，显示模板
 */
app.use(function(err, req, res, next){
    // log it
    if (!module.parent) console.error(err.stack);

    // error page
    res.status(500).render('5xx');
});

// assume 404 since no middleware responded
app.use(function(req, res, next){
    res.status(404).render('404', { url: req.originalUrl });
});
/* istanbul ignore next */
if (!module.parent) {
    app.listen(3000, function () {
        console.log('Express started on port 3000');
    })
}