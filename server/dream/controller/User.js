/**
 * Created by jinzhengkun on 17/1/13.
 * 用户使用api
 */

var httpConfig = require('../util/http-config')
var judgeConfig = require('../util/judgeJS')
var User = require('../model/User')

var jwt = require('jsonwebtoken')
var jwtConfig = require('../jwt/jwt')

var getUsers = function (req, res) {
    var paramsObj = req.query || {}
    User.find(httpConfig.httpGetQuery(paramsObj), function (err, result) {
        if (err) {
            throw err
            res.send(err)
        }
        else {
            res.send(httpConfig.HTTP_STATUS_GET, result)
        }
    })
}

var login = function (req, res) {
    // 登录的时候，这里查询数据库应该先查找有没有这个邮箱注册，或者用户名不存在，之后再验证密码的正确性
    var loginParamsObj = judgeConfig.deepClone(req.body || {})
    var paramsObj = judgeConfig.deepClone(req.body || {})
    User.findOne(httpConfig.loginConfig(paramsObj), function (err, result) {
        if (err) {
            throw err
            res.send(err)
        }
        else if (!result) {
            res.send(httpConfig.HTTP_STATUS_ERROR, httpConfig.noAccountResponse)
        }
        else {
            isLogin(req, res, loginParamsObj)
        }
    })
}
// 验证账户名密码正确性
function isLogin (req, res, paramsObj) {
    User.findOne(paramsObj, function (err, result) {
        if (err) {
            throw err
            res.send(err)
        }
        else if (!result) {
            res.send(httpConfig.HTTP_STATUS_ERROR, httpConfig.passwordErrorResponse)
        }
        else {
            // cao...这里引入secret就验证不过去了，不知道为啥
            var secret = 'crassland_wolf-secret'
            var token = jwtConfig.createToken(result, secret) //jwt.sign(payload, 'crassland_wolf-secret');
            var resResult = {
                status: true,
                message: '登录成功',
                token: token
            }
            res.send(httpConfig.HTTP_STATUS_POST, resResult)
        }
    })
}

/**
 * 注册
 */
function register (req, res) {
    User.insert(req.body || {}, function (err, result) {
        if (err) {
            res.send(err)
        }
        else {
            var secret = 'crassland_wolf-secret'
            var token = jwtConfig.createToken(result, secret) //jwt.sign(payload, 'crassland_wolf-secret');
            var resResult = {
                success: true,
                status: true,
                message: '注册成功',
                _id: result._id,
                token: token
            }
            res.send(httpConfig.HTTP_STATUS_POST, resResult)
        }
    })
}

/**
 * 注册时检查email是否已经存在
 * @param req
 * @param res
 */
function hasAccount(req, res) {
    User.findOne({email: req.body.email}, function (err, result) {
        if (err) {
            res.send(err)
        }
        else if (result) {
            res.send(httpConfig.HTTP_STATUS_POST, httpConfig.hasAccountResponse)
        }
        else {
            res.send(httpConfig.HTTP_STATUS_POST, httpConfig.noHasAccountResponse)
        }
    })
}


module.exports = {
    getUsers: getUsers,
    login: login,
    register: register,
    hasAccount: hasAccount
}