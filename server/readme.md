Read：允许用户读取指定数据库
readWrite：允许用户读写指定数据库
dbAdmin：允许用户在指定数据库中执行管理函数，如索引创建、删除，查看统计或访问system.profile
userAdmin：允许用户向system.users集合写入，可以找指定数据库里创建、删除和管理用户
clusterAdmin：只在admin数据库中可用，赋予用户所有分片和复制集相关函数的管理权限。
readAnyDatabase：只在admin数据库中可用，赋予用户所有数据库的读权限
readWriteAnyDatabase：只在admin数据库中可用，赋予用户所有数据库的读写权限
userAdminAnyDatabase：只在admin数据库中可用，赋予用户所有数据库的userAdmin权限
dbAdminAnyDatabase：只在admin数据库中可用，赋予用户所有数据库的dbAdmin权限。
root：只在admin数据库中可用。超级账号，超级权限


{
  "dependencies": {
    "body-parser": "^1.15.2",
    "crypto": "^0.0.3",
    "express": "^4.14.0",
    "express-session": "^1.14.2",
    "jade": "^1.11.0",
    "jsonwebtoken": "^7.2.1",
    "method-override": "^2.3.7",
    "mongodb": "^2.2.19",
    "mongoose": "^4.7.6"
  }
}
