/**
 * Created by jinzhengkun on 17/1/10.
 */
var MongoClient = require('mongodb').MongoClient;
var DB_CONN_STR = 'mongodb://localhost:27017/dream';

var selectData = function(db, callback) {
    //连接到表
    var collection = db.collection('tb2');
    //查询数据
    var whereStr = {"name":"crassland_wolf"};
    collection.find().toArray(function(err, result) {
        if(err)
        {
            console.log('Error:'+ err);
            return;
        }
        console.log(result)
        callback(result);
    });
}

MongoClient.connect(DB_CONN_STR, function(err, db) {
    console.log("连接成功！");
    selectData(db, function(result) {
        console.log(result);
        db.close();
    });
});

